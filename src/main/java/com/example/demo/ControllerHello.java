package com.example.demo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
@RestController
public class ControllerHello {
    @RequestMapping("/")
    public String index(){
        return "Greetings From Spring Boot!";
    }
}
